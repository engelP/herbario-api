
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('course').del()
    .then(function () {
      // Inserts seed entries
      return knex('course').insert([
        {
          id_categories_course: 1,
          name: 'Herramientas para el cuido de las plantas',
          capacity: 30,
          image: '404',
          start_date: '2020-03-13',
          end_date: '2020-06-13',
          description: 'Este curso te permitirá elegir herramientas de una forma fácil para evitar hacerle daño a tus plantas'
        },
        {
          id_categories_course: 1,
          name: 'Facilidad para identificar tus plantas',
          capacity: 11,
          image: '404',
          start_date: '2020-03-13',
          end_date: '2020-06-13',
          description: 'Identifica plantas de una forma fácil'
        }
      ]);
    });
};
