
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('optionRole').del()
    .then(function () {
      // Inserts seed entries
      return knex('optionRole').insert([
        {id_role: 1, id_option: 1},
        {id_role: 1, id_option: 4},
      ]);
    });
};
