
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('samples').del()
    .then(function () {
      // Inserts seed entries
      return knex('samples').insert([
        {name: 'fir tree', id_species: 1, description: 'scientific name: Abies alba', image: '404', is_active: true},
        {name: 'Abelia', id_species: 2, description: 'this shrub resists green all year round if well protected', image: '404', is_active: true},
        {name: 'coriander', id_species: 3, description: 'seeds are used and even, in some places, even their roots', image: '404', is_active: true},
      ]);
    });
};
