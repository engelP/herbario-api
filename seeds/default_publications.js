
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('publications').del()
    .then(function () {
      // Inserts seed entries
      return knex('publications').insert([
        {
          title: 'Nuevas especies descubiertas', 
          content: 'Segun nuestro inestigador Josep Morgan en la region sur del departamento de leon se descubrio una nueva planta llamada batata por los nativos',
          id_categories: 1, image: 'image/batata.png', date: '2020-01-20', id_users: 1
        },
        {
          title: 'Visita guiada', 
          content: 'El Herbario Programo una visita por su arboreton disponible para todo publico, lles invitamos UwU',
          id_categories: 2, image: 'image/Cursos.png', date: '2020-08-16', id_users: 1
        }
      ]);
    });
};
