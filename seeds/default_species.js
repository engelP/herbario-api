
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('species').del()
    .then(function () {
      // Inserts seed entries
      return knex('species').insert([
        {description: 'coniferous', is_active: true},
        {description: 'shrubbery', is_active: true},
        {description: 'herbs', is_active: true}
      ]);
    });
};
