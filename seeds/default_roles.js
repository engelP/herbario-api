
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('roles').del()
    .then(function () {
      // Inserts seed entries
      return knex('roles').insert([
        { description: 'Admin', is_active: true},
        { description: 'Content Creator', is_active: true},
        { description: 'Data Recorder', is_active: true}
      ]);
    });
};
