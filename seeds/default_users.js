
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        { id_rol: 1, first_name: 'Engel', middle_name: 'Enrique', first_surname: 'Pena', second_surname: 'padilla', username: 'Yunita', password: '123', description: 'este es un user'}
      ]);
    });
};
