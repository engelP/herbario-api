
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('options').del()
    .then(function () {
      // Inserts seed entries
      return knex('options').insert([

        {description: 'fetch all Samples', is_active: true, route: '/samples'},
        {description: 'Create Samples', is_active: true, route: '/samples/create'},
        {description: 'Update Samples', is_active: true, route: '/samples/update'},
        {description: 'Delete Samples', is_active: true, route: '/samples/delete'},

        {description: 'fetch all Species', is_active: true, route: '/species'},
        {description: 'Create Species', is_active: true, route: '/species/create'},
        {description: 'Update Species', is_active: true, route: '/species/update'},
        {description: 'Delete Species', is_active: true, route: '/species/delete'},

        {description: 'fetch all Options', is_active: true, route: '/options'},
        {description: 'Create Options', is_active: true, route: '/options/create'},
        {description: 'Update Options', is_active: true, route: '/options/update'},
        {description: 'Delete Options', is_active: true, route: '/options/delete'},

        {description: 'fetch all Categories', is_active: true, route: '/categories'},
        {description: 'Create Categories', is_active: true, route: '/categories/create'},
        {description: 'Update Categories', is_active: true, route: '/categories/update'},
        {description: 'Delete Categories', is_active: true, route: '/categories/delete'},

        {description: 'fetch all Publications', is_active: true, route: '/publications'},
        {description: 'Create Publications', is_active: true, route: '/publications/create'},
        {description: 'Update Publications', is_active: true, route: '/publications/update'},
        {description: 'Delete Publications', is_active: true, route: '/publications/delete'},

        {description: 'fetch all Users', is_active: true, route: '/users'},
        {description: 'Create Users', is_active: true, route: '/users/create'},
        {description: 'Update Users', is_active: true, route: '/users/update'},
        {description: 'Delete Users', is_active: true, route: '/users/delete'},

        {description: 'fetch all Roles', is_active: true, route: '/roles'},
        {description: 'Create Roles', is_active: true, route: '/roles/create'},
        {description: 'Update Roles', is_active: true, route: '/roles/update'},
        {description: 'Delete Roles', is_active: true, route: '/roles/delete'},

        {description: 'fetch all Course Category', is_active: true, route: '/courseCategory'},
        {description: 'Create Course Category', is_active: true, route: '/courseCategory/create'},
        {description: 'Update Course Category', is_active: true, route: '/courseCategory/update'},
        {description: 'Delete Course Category', is_active: true, route: '/courseCategory/delete'},

        {description: 'fetch all Course', is_active: true, route: '/course'},
        {description: 'Create Course', is_active: true, route: '/course/create'},
        {description: 'Update Course', is_active: true, route: '/course/update'},
        {description: 'Delete Course', is_active: true, route: '/course/delete'}

      ]);
    });
};
