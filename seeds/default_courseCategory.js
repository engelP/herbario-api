
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('coursecategory').del()
    .then(function () {
      // Inserts seed entries
      return knex('coursecategory').insert([
        {description: 'identification and classsification', is_active: true},
        {description: 'harvest', is_active: true},
        {description: 'analysis', is_active: true},
      ]);
    });
};
