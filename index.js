const express = require("express");
const bodyParse = require("body-parser");
const dotEnv = require("dotenv");
const morgan = require("morgan");
const cookieParser = require('cookie-parser');

const knexConfig = require("./knexfile");
const Knex = require("knex");
const { Model } = require("objection");

dotEnv.config({path: "./config.env"});

const verifyToken = require('./middleware/verifyToken');
const permissions = require('./middleware/permissions');

const speciesController = require('./controllers/speciesController');
const samplesController = require('./controllers/samplesController');
const optionsController = require('./controllers/optionsController');
const categoriesController = require('./controllers/categoriesController');
const publicationsController = require('./controllers/publicationsController');
const usersController = require('./controllers/usersController');
const rolesController = require('./controllers/rolesController');
const courseCategoryController = require('./controllers/courseCategoryController');
const coursesController = require('./controllers/coursesController');
const institution = require('./controllers/institutionController');
const optionsRole = require('./controllers/optionsRoleController');

//Configuring objection
const knex = Knex(knexConfig.development);
Model.knex(knex);

//configuration completed
const app = express();

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extend:false}));

//Species controller
app.use("/species", verifyToken);
app.use("/species", permissions);
app.use("/species", speciesController);

app.use("/samples", verifyToken);
app.use("/samples", permissions);
app.use("/samples", samplesController);

app.use("/options", optionsController);
app.use("/categories", categoriesController);
app.use("/publications", publicationsController);
app.use("/users", usersController);
app.use("/roles", rolesController);
app.use("/courseCategory", courseCategoryController);
app.use("/course", coursesController);
app.use("/optionsRole", optionsRole);
app.use("/institution", institution);

app.use((req, res) => {
    res.status(404).send({ error: 'page not found' });
});

//errors
app.use((err, req, res) => {
    console.log(err);
    res.status(err.status || 500);
    res.send({
        message: err.message,
        errors: err.errors,
    });
});
 
app.listen(process.env.PORT || 3000, _ => {
    console.clear();
    console.log(`Server is running on port ${process.env.PORT}`);
});