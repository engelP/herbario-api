// Update with your config settings.

module.exports = {
  development: {
    client: 'mysql',
    connection: {
      database: 'herbario_bueno',
      user:     'root',
      password: ''
    },    
    migrations: {
      directory: __dirname + "/migrations",
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: __dirname + "/seeds"
    }
  },

  staging: {
    client: 'mysql',
    connection: {
      database: 'herbario_bueno',
      user:     'root',
      password: ''
    },    
    migrations: {
      directory: __dirname + "/migrations",
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'mysql',
    connection: {
      database: 'herbario_bueno',
      user:     'root',
      password: ''
    },    
    migrations: {
      directory: __dirname + "/migrations",
      tableName: 'knex_migrations'
    }
  }
};
