const { Model } = require('objection');

class Category extends Model{
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'categories'
    }

    static get jsonSchema(){
        return {
            type: 'object',
            required: ['description'],

            properties: {
                description: { type: 'string' }
            }
        }
    }

    static get relationMappings(){
        const Publication = require('./publicationsModel');

        return{
            publication: {
                relation: Model.HasManyRelation,
                modelClass: Publication,

                join: {
                    from: "categories.id",
                    to: 'publications.id_categories'
                }
            }
        }
    }

    fetchAll = async () => {
        const categories = await Category.query().withGraphFetched('publication');
        return categories;
    }

    create = async (data) => {
        try {
            await Category.query().insert(data);
            return true;
        } catch (error) {
            return false
        }
    }

    update = async (id, data) => {
        try {
            await Category.query().findById(id).patch(data);    
           return true
        } catch (error) {
            return false
        }
    }

    delete = async (id) => {
        try {
            await Category.query().findById(id).patch({
               is_active: 0 
            });
            
            return true;
        } catch (error) {
            return false;
        }
    }

    find = async (id) => {
        try {
            const category = await Category.query().findById(id).withGraphFetched('publication');
            return category;
        } catch (error) {
            return false;
        }
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    }
}

module.exports = Category;