const { Model } = require('objection');

class Samples extends Model {
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'samples';
    }

    static get jsonSchema(){
        return {
            type: 'object',
            required: ['name', 'id_species', 'description', 'image'],

            properties: {
                name: { type: 'string' },
                id_species: { type: 'integer' },
                description: { type: 'string' },
                image: { type: 'string' }
            }
        }
    }

    static get relationMappings(){
        const Species = require('./speciesModels');

        return{
            species: {
                relation: Model.BelongsToOneRelation,
                modelClass: Species,

                join: {
                    from: 'samples.id_species',
                    to: 'species.id '
                }
            }
        }
    }

    fetchAll = async () => {
        const found = await Samples.query().withGraphFetched('species');
        return found;
    }
    
    create = async (data) => {
        try {
            await Samples.query().insert(data);
            return true;
        } catch (error) {
            return false
        }
    }

    update = async (id, data) => {
        try {
            await Samples.query().findById(id).patch(data);    
           return true
        } catch (error) {
            return false
        }
    }

    delete = async (id) => {
        try {
            await Samples.query().findById(id).patch({
                is_active: 0
            });

            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    find = async (id) => {
        try {
            const result = await Samples.query().findById(id).withGraphFetched('species');
            return result;
        } catch (error) {
            return false
        }
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    } 
}

module.exports = Samples;
