const { Model } = require('objection');

class CourseCategory extends Model {
    static get idColumn() {
        return 'id';
    }

    static get tableName() {
        return 'coursecategory';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['description'],

            properties: {
                description: { type: 'string'}
            }
        }
    }

    static get relationMappings(){
        const Course = require('./courseModel');

        return {
            course : {
                relation: Model.HasManyRelation,
                modelClass: Course,

                join: {
                    from: "coursecategory.id",
                    to: "course.id_categories_course"
                }
            }
        }
    }

    fetchAll = async () => {
        const courseCategory = await CourseCategory.query().withGraphFetched('course');
        return courseCategory;
    }

    create = async (data) => {
        try {
            await CourseCategory.query().insert(data);
            return true;
        } catch (error) {
            return false;
        }
    }

    update = async (id, description) => {
        try {
            await CourseCategory.query().findById(id).patch({
                description
            });
            return true
        } catch (error) {
            return false
        }
    }

    delete = async (id) => {
        try {
            await CourseCategory.query().findById(id).patch({
                is_active: 0
            });
            return true;
        } catch (error) {
            return false;
        }
    }

    find = async (id) => {
        const result = await CourseCategory.query().findById(id).withGraphFetched('course');
        return result;
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    }
}

module.exports = CourseCategory;