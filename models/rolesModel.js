const { Model } = require('objection');
const OptionModel = require('./optionsModel');

class Role extends Model{
    static get idColumn(){
        return 'id';
    }

    static get tableName() {
        return 'roles';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['description'],

            properties: {
                description: { type: 'string' }
            }
        }
    }

    static relationMappings = {
        options: {
            relation: Model.ManyToManyRelation,
            modelClass: OptionModel,
            join: {
                from: "roles.id",
                through: {
                    from: "optionrole.id_role",
                    to: "optionrole.id_option"
                },
                to: "options.id"
            },
        }
    }

    fetchAll = async () => {
        const roles = await Role.query().withGraphFetched('options');
        return roles;
    }

    create = async (data) => {
        try {
            await Role.query().insert(data);
            return true;
        } catch (error) {
            return false;
        }
    }

    update = async (id, data) => {
        try {
            await Role.query().findById(id).patch(data);    
           return true
        } catch (error) {
            return false
        }
    }

    delete = async (id) => {
        try {
           await Role.query().findById(id).patch({
                is_active : 0
           }) 
           return true
        } catch (error) {
            return false
        }
    }

    find = async (id) => {
        try {
           const result = await Role.query().findById(id).withGraphFetched('options');
           return result;
        } catch (error) {
            return false;
        }
    }

    addOption = async(optionId, rolId) => {
        try {
            await Role.relatedQuery('options').for(rolId).relate(optionId);
            return true;            
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    } 
}

module.exports = Role;