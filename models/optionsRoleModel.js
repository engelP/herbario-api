const { Model } = require('objection');

class OptionsRole extends Model {
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'optionrole';
    }

    static get jsonSchema(){
        return {
            type: 'object',
            required: ['id_role', 'id_option' ],

            properties: {
                id_role: { type: 'integer' },
                id_option: { type: 'integer' }
            }
        }
    }  
    
    fetchAll = async () => {
        const optionRole = await OptionsRole.query();
        return optionRole;
    }

    create = async (data) => {
        try {
            await OptionsRole.query().insert(data);
            return true;
        } catch (error) {
            console.log("Failed to save");
            return false;
        }
    }

    update = async (id, data) => {
        try {
            await OptionsRole.query().findById(id).patch(data);
            return true;
        } catch (error) {
            return false;
        }   
    }

    delete = async id => {
        try {
            await OptionsRole.query().findById(id).patch({
                is_active: 0
            });

            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    find = async id => {
        const result = await OptionsRole.query().findById(id);

        return result;
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    }
}

module.exports = OptionsRole;