const { Model } = require('objection');

class Publication extends Model {
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'publications';
    }

    static get jsonSchema(){
        return {
            type: 'object',
            required: ['title', 'content', 'id_categories', 'image', 'date', 'id_users'],

            properties: {
                title: { type: 'string' },
                content: { type: 'string' },
                id_categories: { type: 'integer' },
                image: { type: 'string' },
                date: { type: 'date' },
                id_users: { type: 'integer' }
            }  
        }
    }

    static get relationMappings(){
        const Categories = require('./categoriesModel');
        const User = require('./usersModel');

        return{
            categories: {
                relation: Model.BelongsToOneRelation,
                modelClass: Categories,

                join: {
                    from: 'publications.id_categories',
                    to: 'categories.id'
                }
            },
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,

                join: {
                    from: 'publications.id_users',
                    to: 'users.id'
                }

            }
        }
    }

    fetchAll = async () => {
        const found = await Publication.query().withGraphFetched({categories: true, user: true});
        return found;
    }

    create = async (data) => {
        try {
            await Publication.query().insert(data);
            return true;
        } catch (error) {
            return false
        }
    }

    update = async (id, data) => {
        try {
            await Publication.query().findById(id).patch(data);    
           return true
        } catch (error) {
            return false
        }
    }

    delete = async (id) => {
        try {
            await Publication.query().findById(id).patch({
                is_active: 0
            });

            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    find = async (id) => {
        try {
            const result = await Publication.query().findById(id).withGraphFetched({categories: true, user: true});
            return result;
        } catch (error) {
            return false
        }
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    }
}

module.exports = Publication;