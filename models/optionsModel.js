const { Model } = require('objection');

class Option extends Model{
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'options';
    }

    static get jsonSchema(){
        return {
            type: 'object',
            required: ['description'],

            properties: {
                description: { type: 'string' }
            }
        }
    }

    create = async (data) => {
        try {
            await Option.query().insert(data);
            return true;
        } catch (error) {
            return false
        }
    }

    update = async (id, data) => {
        try {
            await Option.query().findById(id).patch(data);    
           return true
        } catch (error) {
            return false
        }
    }

    delete = async id => {
        try {
            await Option.query().findById(id).patch({
               is_active: 0 
            });
            
            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    find = async id => {
        const result = await Option.query().findById(id);

        return result;
    }

    fetchAll = async _ => {
        const result = await Option.query();

        return result;
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    } 
}

module.exports = Option;