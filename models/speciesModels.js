const { Model } = require('objection');

class Species extends Model {
    static get idColumn() {
        return 'id';
    }

    static get tableName() {
        return 'species';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['description'],

            properties: {
                description: { type: 'string' }
            }
        }
    }

    static get relationMappings(){
        const Samples = require('./samplesModel');

        return{
            samples: {
                relation: Model.HasManyRelation,
                modelClass: Samples,

                join: {
                    from: "species.id",
                    to: 'samples.id_species'
                }
            }
        }
    }

    fetchAll = async () => {
        const species = await Species.query().withGraphFetched('samples');
        return species;
    }

    create = async (data) => {
        try {
            await Species.query().insert(data);
            return true;
        } catch (error) {
            return false
        }
    }

    update = async (id, data) => {
        try {
            await Species.query().findById(id).patch(data);    
           return true
        } catch (error) {
            return false
        }
    }

    delete = async (id) => {
        try {
           await Species.query().findById(id).patch({
                is_active : 0
           }) 
           return true
        } catch (error) {
            return false
        }
    }

    find = async (id) => {
        try {
           const specie = await Species.query().findById(id).withGraphFetched('samples');
           return specie;
        } catch (error) {
            return false;
        }
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    } 
}

module.exports = Species;