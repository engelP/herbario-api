const { Model } = require('objection');

class Course extends Model {
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'course';
    }

    static get jsonSchema(){
        return {
            type: 'object',
            required: ['id_categories_course', 'name', 'capacity', 'image', 'start_date', 'end_date', 'description'],

            properties: {
                id_categories_course: { type: 'integer' },
                name: { type: 'string'},
                capacity: { type: 'integer' },
                image: { type: 'string' },
                start_date: { type: 'date' },
                end_date: { type: 'date' },
                description: { type: 'string' }
            }
        }
    }

    static get relationMappings(){
        const CourseCategory = require('./courseCategoryModel');

        return {
            courseCategory: {
                relation: Model.BelongsToOneRelation,
                modelClass: CourseCategory,

                join: {
                    from: "course.id_categories_course",
                    to: "coursecategory.id"
                }
            }
        }
    }

    create = async (course) => {
        try {
            await Course.query().insert(course);
            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    update = async (courseUpate, id) => {
        try {
            await Course.query().findById(id).patch(courseUpate);
            return true;
        } catch (error) {
            return false;
        }   
    }

    delete = async id => {
        try {
            await Course.query().findById(id).patch({
                is_active: 0
            });

            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    find = async id => {
        const result = await Course.query().findById(id).withGraphFetched('courseCategory');
        return result;
    }

    fetchAll = async () => {
        const results = await Course.query().withGraphFetched('courseCategory');
        return results;
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    }

}

module.exports = Course;