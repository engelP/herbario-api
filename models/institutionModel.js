const { Model } = require('objection');

class Institution extends Model {
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'institution';
    }

    static get jsonSchema(){
        return {
            type: 'object',
            required: ['name', 'mission', 'view', 'description'],

            properties: {
                name: { type: 'string'},
                mission: { type: 'string' },
                view: { type: 'string' },
                description: { type: 'string' }
            }

        }
    }

    fetchAll = async () => {
        const institution = await Institution.query();
        return institution;
    }

    create = async (data) => {
        try {
            await Institution.query().insert(data);
            return true;
        } catch (error) {
            return false
        }
    }

    update = async (id, data) => {
        try {
            await Institution.query().findById(id).patch(data);    
           return true
        } catch (error) {
            return false
        }
    }

    delete = async id => {
        try {
            await Institution.query().findById(id).patch({
                is_active: 0
            });

            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    find = async (id) => {
        const result = await Institution.query().findById(id);
        return result;
    }

    async $beforeInsert(queryContext) {
        await super.$beforeInsert(queryContext);
        this.created_at = new Date();
    }
    
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.updated_at = new Date();
    } 
}

module.exports = Institution;