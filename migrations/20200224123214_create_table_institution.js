
exports.up = function(knex) {
    return knex.schema.createTable("institution", table => {
        table.increments("id").primary();
        table.text("name").notNullable();
        table.text("mission").notNullable();
        table.text("view").notNullable();
        table.text("description").notNullable();
        table.timestamps();
        table.boolean("is_active").defaultTo(true);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("institution");
};
