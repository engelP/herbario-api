
exports.up = function(knex) {
  return knex.schema.createTable("samples", table =>{
     table.increments("id").primary();
     table.text("name").notNullable();
     table.integer("id_species").notNullable();
     table.text("description").notNullable();
     table.text("image").notNullable();
     table.timestamps();
     table.boolean("is_active").defaultTo(true).notNullable();
  })
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists("samples");
};
