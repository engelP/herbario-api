
exports.up = function(knex) {
    return knex.schema.createTable("publications", table => {
        table.increments("id").primary();
        table.text("title").notNullable();
        table.text("content").notNullable();
        table.integer("id_categories").notNullable();
        table.text("image").notNullable();
        table.date("date").notNullable();
        table.integer("id_users").notNullable();
        table.timestamps();
        table.boolean("is_active").defaultTo(true);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("publications");
};
