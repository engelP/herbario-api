
exports.up = function(knex) {
    return knex.schema.createTable("optionRole", table =>{
        table.increments("id").primary();
        table.integer("id_role").notNullable();
        table.integer("id_option").notNullable();
        table.boolean("is_active").defaultTo(true);
     })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("optionRole");
};
