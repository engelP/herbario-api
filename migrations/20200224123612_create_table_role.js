
exports.up = function (knex) {
    return knex.schema.createTable("roles", table => {
        table.increments("id").primary();
        table.text("description").notNullable();
        table.timestamps();
        table.boolean("is_active").defaultTo(true);
    })
};

exports.down = function (knex) {
    return knex.schema.dropTableIfExists("roles");
};
