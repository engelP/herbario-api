
exports.up = function(knex) {
    return knex.schema.createTable("course", table => {
        table.increments("id").primary();
        table.integer("id_categories_course").notNullable();
        table.text("name").notNullable();
        table.integer("capacity").notNullable();
        table.text("image").notNullable();
        table.date("start_date").notNullable();
        table.date("end_date").notNullable();
        table.text("description").notNullable();
        table.boolean("is_active").defaultTo(true);
        table.timestamps();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("course");
};
