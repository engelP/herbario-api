const express = require('express');
const Publication = require('../models/publicationsModel');
const asyncWrapper = require('../middleware/asyncWrap');

const router = express.Router();

router.get("/", asyncWrapper(async (req, res) => {
    const publication = new Publication();
    const result = await publication.fetchAll();

    if (result.length) {
        return res.status(200).send({ data: result });
    }
 
    return res.status(404).send({ error: "not found" });
}));

router.get("/:id", asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const publication = new Publication();
    const result = await publication.find(id);

    if(result){
        return res.status(200).send({ data : result});
    }

    return res.status(404).send({error: "Not found"});
}));

router.post("/create", asyncWrapper(async (req, res) => {
    const publication = new Publication();
    const result = await publication.create(req.body);

    if (result) {
        return res.status(200).send({ message: "Created" });
    }

    return res.status(422).send({ error: "Failed to save" });
}));

router.post("/update/:id", asyncWrapper(async (req, res) => {
    const publication = new Publication();
    const { id } = req.params;
    const result = await publication.update(id, req.body);

    if(result){
        return res.status(200).send({message: "updated"});
    }

    return res.status(422).send({error:"Failed to updated"});
}));

router.post("/delete/:id", asyncWrapper(async (req, res) => {
    const publication = new Publication();
    const { id } = req.params;
    const result = await publication.delete(id);

    if(result){
        return res.status(200).send({ message: "record removed"});
    }

    return res.status(404).send({ error: "failed to removed"});
}));

module.exports = router;