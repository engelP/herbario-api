const express = require('express');
const Course = require('../models/courseModel');
const asyncWrapper = require('../middleware/asyncWrap');
const permissions = require('../middleware/permissions');

const router = express.Router();

router.post("/create", asyncWrapper(async (req, res) => {
    const course = new Course();
    const result = await course.create(req.body);

    if(result){
        return res.status(200).send({ message: "Created"});
    }

    return res.status(402).send({error: "Failed to save"});
}));

router.post("/update/:id", asyncWrapper(async (req, res) => {
    const course = new Course();
    const { id } = req.params;
 
    const result = await course.update(req.body, id);

    if(result){
        return res.status(200).send({ message: "updated succesful"});
    }

    return res.status(422).send({error: "Falied to update"});
}));

router.post("/delete/:id", asyncWrapper(async (req, res) => {
    const course = new Course();
    const { id } = req.params;

    const result = await course.delete(id);

    if(result){
        return res.status(200).send({ message: "record removed"});
    }

    return res.status(404).send({ error: "failed to removed" });
}));

router.get("/:id", asyncWrapper(async(req, res) => {
    const course = new Course();
    const { id } = req.params;

    const result = await course.find(id);

    if(result){
        return res.status(200).send({ data: result });
    }

    return res.status(404).send({error: "Not found"});
}));

router.get("/", async(req, res) => {
    const course = new Course();
    const result = await course.fetchAll();

    if(result.length){
        return res.status(200).send({ data: result });
    }

    return res.status(404).send({ error: "not found" });
})

module.exports = router;