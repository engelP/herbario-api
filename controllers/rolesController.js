const express = require('express');
const Roles = require('../models/rolesModel');
const asyncWrapper = require('../middleware/asyncWrap');

const router = express.Router();

router.get("/", asyncWrapper(async (req, res) => {
    const roles = new Roles();
    const result = await roles.fetchAll();

    if (result.length) {
        return res.status(200).send({ data: result });
    }

    return res.status(404).send({ error: "not found" });
}));

router.get("/:id", asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const roles = new Roles();
    const result = await roles.find(id);

   if(result){
       return res.status(200).send({ data: result });
   }
   return res.status(404).send({error: "Not found"});
}));

router.post("/create", asyncWrapper(async (req, res) => {
    const roles = new Roles();
    const result = await roles.create(req.body);
    console.log(result);
    if (result) {
        return res.status(200).send({ message: "Created" });
    }

    return res.status(422).send({ error: "Failed to save" });
}));

router.post("/update/:id", asyncWrapper(async (req, res) => {
    const roles = new Roles();
    const { id } = req.params;
    const result = await roles.update(id, req.body);

    if(result){
        return res.status(200).send({ message: "updated succesful"});
    }

    return res.status(422).send({error: "Falied to update"});
}));

router.post("/delete/:id", asyncWrapper(async (req, res) => {
    const roles = new Roles();
    const { id } = req.params;
    const result = await roles.delete(id);

    if(result){
        return res.status(200).send({ message: "record removed"});
    }
    return res.status(404).send({ error: "failed to removed" });
}));

router.post("/options", asyncWrapper(async (req, res) => {
    const { rolId, optionId } = req.body;
    const roles = new Roles();

    const success = roles.addOption(optionId, rolId);

    if (success) {
        return res.status(200).send({ "message": "Option added" });
    }

    return res.status(422).send({ error: "Failed to save" });
}));

module.exports = router;