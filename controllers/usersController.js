const express = require('express');
const User = require('../models/usersModel');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const asyncWrapper = require('../middleware/asyncWrap');
const verifyToken = require('../middleware/verifyToken');
const permissions = require('../middleware/permissions');

const router = express.Router();

router.get("/", verifyToken, permissions, asyncWrapper(async (req, res) => {
    const { token } = req;
    const user = new User();
    const result = await user.fetchAll();

    if(result.length){
        return res.status(200).send({ data: result, token});
    }
        
    return res.status(404).send({ error: "not found" });
}));

router.get("/:id", asyncWrapper(async(req, res) => {
    const { id } = req.params;
    const user = new User();
    const result = await user.find(id)

    if(result){
        return res.status(200).send({ data: result });
    }
    return res.status(404).send({error: "Not found"});
}));

router.post("/create", asyncWrapper(async (req, res) => {    
    const user = new User();
    const result = await user.create(req.body);
    
    if(result) {
        return res.status(200).send({ message: "Created"});
    }
    
    return res.status(422).send({ error: "Failed to save" });    
}));

router.post("/update/:id", asyncWrapper(async (req, res) => {
    const user = new User();
    const { id } = req.params;
    const result = await user.update(req.body, id);
    
    if(result){
        return res.status(200).send({ message: "updated succesful"});
    }

    return res.status(422).send({error: "Falied to update"});
}));

router.post("/delete/:id", asyncWrapper(async (req, res) => {
    const user = new User();
    const { id } = req.params;
    const result = await user.delete(id);

    if(result){
        return res.status(400).send({ message: "record removed"});
    }

    return res.status(404).send({ error: "failed to removed" });
}));

router.post("/login", asyncWrapper (async (req, res) => {
    const user = new User();
    const {username, password} = req.body;
    const result = await user.verifyCredentials(username, password);

    if(!result){
        throw { status: 404, message: "Invalid Credentials" }
    }       

    const options = {
        issuer: "SOX",
        subject: `${result.first_name} ${result.first_surname}`,
        audience: "https://soxherbario.com",
        expiresIn: "1h",
        algorithm: "RS256"
    }

    const payload = {
        username, password: result.password, user_id: result.id
    }

    const privateKey = fs.readFileSync("./private.key", "utf8");
    const token = await jwt.sign(payload, privateKey, options);

    res.cookie("token", token);
    return res.status(200).send({token});
}));

module.exports = router;