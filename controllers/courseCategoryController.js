const express = require('express');
const CourseCategory = require('../models/courseCategoryModel');
const asyncWrap = require('../middleware/asyncWrap');

const router = express.Router();

router.get("/", asyncWrap(async (req, res) => {
    const courseCategory = new CourseCategory();

    const data = await courseCategory.fetchAll();
    console.log(data);

    if(data.length){
        return res.status(200).send({data});
    }

    return res.status(404).send({ error: "data not found" });

}));

router.get("/:id", asyncWrap(async (req, res) => {
    const { id } = req.params;
    const courseCategory = new CourseCategory();
    const data = await courseCategory.find(id);

    if(data){
        return res.status(200).send({data});
    }

    return res.status(404).send({error: "not found"});
}));

router.post("/create", asyncWrap(async (req, res) => {
    const courseCategory = new CourseCategory();
    const courseCategoryJson = req.body;
    const result = await courseCategory.create(courseCategoryJson);

    if(result){
        return res.status(200).send({message: "Saved"})
    }

    return res.status(402).send({ error: "failed to save" });
}));

router.post("/update/:id", asyncWrap(async (req, res) => {
        const { description } = req.body;
        const { id } = req.params;
        const courseCategory = new CourseCategory();
        const result = await courseCategory.update(id, description);
    
        if(result){
            return res.status(200).send({ message: "Updated"});
        }
    
        return res.status(500).send({error: "failed to update"});
    }
));

router.post("/delete/:id", asyncWrap(async (req, res) => {
    const { id } = req.params;
    const courseCategory = new CourseCategory();
    const result = await courseCategory.delete(id);

    if(result){
        return res.status(200).send({ message: "Delete"});
    }

    return res.status(500).send({error: "failed to delete"});    
}));
module.exports = router;