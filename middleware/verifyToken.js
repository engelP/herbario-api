const fs = require("fs");
const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
    const header = req.headers["authorization"];
    const { token: cookieToken } = req.cookies;

    if(!header && !cookieToken){
        return res.status(403).send({ error:"Unauthorized"});
    }

    const headerToken = header ? header.split(" ")[1] : null;    
    const publicKey = fs.readFileSync("./public.key", "utf8");

    const options = {
        issuer: "SOX",
        audience: "https://soxherbario.com",
        expiresIn: "1h",
        algorithm: "RS256"
    }

    const token = cookieToken || headerToken;

    jwt.verify(token, publicKey, options, async (err, data) => {
        if(err){
            return res.status(403).send({error: "Unauthorized"});
        }else {
            
            const options = {
                issuer: "SOX",
                subject: data.sub,
                audience: "https://soxherbario.com",
                expiresIn: "1h",
                algorithm: "RS256"
            }
        
            const payload = {
                username: data.username, password: data.password, user_id: data.user_id
            }
        
            const privateKey = fs.readFileSync("./private.key", "utf8");
            const token = await jwt.sign(payload, privateKey, options);
        
            req.user_id = data.user_id;
            req.token = token;
            next();
        }
    });
}

module.exports = verifyToken;
